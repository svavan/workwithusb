package exp.vladimir.shulika.workwithusb;

import android.hardware.usb.UsbManager;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

public class MainActivity extends AppCompatActivity {

    TextView out;
    Button buttonPrint,buttonLoadFonts, buttonGetFileList, buttonExecAdvancedCommand, buttonDellAllFiles;

        @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        out = (TextView) findViewById(R.id.out);
        buttonPrint = (Button) findViewById(R.id.buttonPrint);
        buttonLoadFonts = (Button) findViewById(R.id.buttonLoadFonts);
        buttonGetFileList = (Button) findViewById(R.id.buttonGetFileList);
        buttonExecAdvancedCommand = (Button) findViewById(R.id.buttonExecAdvancedCommand);
        buttonDellAllFiles = (Button) findViewById(R.id.buttonDellAllFiles);

        DataTransfer df = new TscTDP225USBPort(0x1203,0x0160,(UsbManager) getSystemService(MainActivity.USB_SERVICE));

        final TscTDP225 tscTDP225 = new TscTDP225(df, MainActivity.this);

            try
            {
                df.open();
            }
            catch (Exception ex)
            {
                Log.d("TscTDP225", "Error(construct): " + ex.toString());
            };

        buttonPrint.setOnClickListener(new View.OnClickListener() {
                                           public void onClick(View v)
                                           {
                                               tscTDP225.testLabel();
                                               tscTDP225.writeUSB();

                                           }

                                       }
        );


            buttonGetFileList.setOnClickListener(new View.OnClickListener() {
                                                   public void onClick(View v)
                                                   {
                                                       out.append("\n\r\n" + tscTDP225.getFileList());
                                                       out.append("\n\r\nFonts search: ");
                                                       if(tscTDP225.checkAllFontExist())
                                                       {out.append("all ok!");}
                                                       else
                                                       {out.append("not all ok (");}

                                                       out.append("\r\n\nFree space(bytes): " + tscTDP225.getFreeSpaceOnFlash());
                                                   }

                                               }
            );


            buttonLoadFonts.setOnClickListener(new View.OnClickListener() {
                                               public void onClick(View v)
                                               {
                                                   if(tscTDP225.loadFontsInTDP225())
                                                       out.append("Load Font ok!");

                                               }

                                           }
            );

            buttonExecAdvancedCommand.setOnClickListener(new View.OnClickListener() {
                                                   public void onClick(View v)
                                                   {
                                                       out.append(tscTDP225.execAdvancedCommand("DIAGNOSTIC FILE \"MSSaSe8.fnt\""));

                                                   }

                                               }
            );

            buttonDellAllFiles.setOnClickListener(new View.OnClickListener() {
                                                      public void onClick(View v)
                                                      {
                                                          out.append(tscTDP225.dellAllFiles());

                                                      }

                                                  }
            );

            //UsbCommunication usbF = new UsbCommunication((UsbManager) getSystemService(Context.USB_SERVICE));
            //UsbCommunication usbF = new UsbCommunication(MainActivity.this);
        //out.append(usbF.getUsbDeviceList());
        //out.append(tscTDP225.getFileList().toString());
        //tscTDP225.createCMDPrintDmatrixBarcode(10,10,100,100,"О-па-па!");
        //tscTDP225.createCMDPrintQRCode("zmr2644@gmail.com",4);
        //tscTDP225.createMyLabel();
        //out.append(tscTDP225.lastCMD);
    }
//====================================================================================



}
