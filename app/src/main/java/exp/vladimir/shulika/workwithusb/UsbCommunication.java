package exp.vladimir.shulika.workwithusb;

import android.app.Activity;
import android.content.Context;
import android.hardware.usb.*;
import android.util.Log;

import java.nio.ByteBuffer;

import static android.hardware.usb.UsbConstants.USB_DIR_IN;
import static android.hardware.usb.UsbConstants.USB_DIR_OUT;

/**
 * Created by shulika on 28.11.2016.
 */
public class UsbCommunication {

    UsbManager mUsbManager;
    Context context;
    UsbDevice UD;

    UsbCommunication(Context cntxt){
        context = cntxt;
        mUsbManager = (UsbManager) context.getSystemService(Context.USB_SERVICE);
    }

    //==============================================================================================
    public UsbDevice findUsbDevice(int vendorId, int productId) {

        // PL2303 0x067B,0x03EF

        //mUsbManager = (UsbManager) getSystemService(Context.USB_SERVICE);

        UsbDevice res = null;

        for (UsbDevice usbDevice : mUsbManager.getDeviceList().values()) {
            if ((usbDevice.getVendorId() == vendorId) && (usbDevice.getProductId() == productId))
                res = usbDevice;
        }
        UD = res;
        return (res);
    }
    //==============================================================================================

    public String getUsbDeviceList () {

        String res="";

        for (UsbDevice usbDevice : mUsbManager.getDeviceList().values()) {
            res = res + "\n"
                    + usbDevice.getDeviceName()
                    + ";  "
                    + "DevClass "
                    + usbDevice.getDeviceClass()
                    + ";  VEN : DEV " + usbDevice.getVendorId()
                    + ":" + usbDevice.getProductId();
        }

        return res;
    }
    //=================================================================================================
    public int deviceEndpointBulkTransfer (UsbDevice UD,
                                            int endPoint,
                                            int timeout,
                                            boolean forceClaim,
                                            byte [] bytes)
    {

        UsbInterface intf = UD.getInterface(0);
        UsbEndpoint ep = intf.getEndpoint(endPoint);
        UsbDeviceConnection connection = mUsbManager.openDevice(UD);

        int direction = ep.getDirection();

        int countByte = 0;

        if(direction== USB_DIR_OUT)
        {

            connection.claimInterface(intf, forceClaim);
            countByte = connection.bulkTransfer(ep, bytes, bytes.length, timeout); //do in another thread

            startDataReceive();
        }

        if(direction== USB_DIR_IN)
        {
            startDataReceive();
        }

        return countByte;
    }
    //=================================================================================================
    public void startDataReceive() {
        new Thread(new Runnable() {

            @Override
            public void run() {

                UsbInterface intf = UD.getInterface(0);
                UsbEndpoint ep = intf.getEndpoint(1);
                UsbDeviceConnection connection = mUsbManager.openDevice(UD);

                int countByte = 0;

                connection.claimInterface(intf, true);

                byte bytes[] = new byte[64];

                while (countByte<1)
                    countByte = connection.bulkTransfer(ep, bytes, bytes.length, 100); //do in another thread


                countByte++;
                /*UsbEndpoint endpoint = null;

                for (int i = 0; i < intf.getEndpointCount(); i++) {
                    if (intf.getEndpoint(i).getDirection() == UsbConstants.USB_DIR_IN) {
                        endpoint = intf.getEndpoint(i);
                        break;
                    }
                }
                UsbRequest request = new UsbRequest(); // create an URB
                boolean initilzed = request.initialize(connection, endpoint);

                if (!initilzed) {
                    Log.e("USB CONNECTION FAILED", "Request initialization failed for reading");
                    return;
                }
                while (true) {
                    int bufferMaxLength = endpoint.getMaxPacketSize();
                    ByteBuffer buffer = ByteBuffer.allocate(bufferMaxLength);


                    if (request.queue(buffer, bufferMaxLength) == true) {
                        if (connection.requestWait() == request) {
                            String result = new String(buffer.array());
                            Log.i("GELEN DATA : ", result);
                            listener.readData(result);
                        }
                    }
                }*/

            }
        }).start();
    }
}
