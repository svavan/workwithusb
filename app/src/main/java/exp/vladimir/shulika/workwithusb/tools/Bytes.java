package exp.vladimir.shulika.workwithusb.tools;

import android.support.annotation.NonNull;

import java.io.UnsupportedEncodingException;
import java.nio.ByteBuffer;
import java.nio.ByteOrder;
import java.util.Arrays;

/**
 * Разное
 */
public class Bytes {
    /**
     * Размер целого в байтах
     */
    private static final int SIZE_INT                = 4;

    /**
     * Размер длинного в байтах
     */
    private static final int SIZE_LONG               = 8;

    /**
     * Сообщение об ошибочной кодировке
     */
    static final String BAD_ENCODING_STRING  = "BAD ENCODING";

    /**
     * Преобразует массив байт в целое число
     * @param data Массив байт
     * @param offset Смещение в массиве
     * @param order Порядок следования бит
     * @param size Размер считываемого блока
     * @return Целое число
     */
    public static int bytesToInt(byte[] data, int offset, int size, ByteOrder order) {
        if (data == null)
            return 0;
        if (size < 0 || size > SIZE_INT)
            throw new IllegalArgumentException("Size must be between 0 and " + SIZE_INT);
        return ByteBuffer.allocate(SIZE_INT)
                .order(order)
                .put(data, offset, size)
                .getInt(0);
    }

    /**
     * Преобразуем целое число в массив байт
     * @param data Входное значение
     * @param size Количество байт считываемых из целого числа
     * @param order Порядок следования бит
     * @return Массив байт
     */
    public static byte[] intToBytes(int data, int size, ByteOrder order) {
        if (size < 0 || size > SIZE_INT)
            throw new IllegalArgumentException("Size must be between 0 and " + SIZE_INT);
        byte[] buff = ByteBuffer.allocate(SIZE_INT)
                .order(order)
                .putInt(data)
                .array();
        return order == ByteOrder.BIG_ENDIAN ?
                Arrays.copyOfRange(buff, (SIZE_INT - 1) - (size - 1), SIZE_INT) :
                Arrays.copyOfRange(buff, 0, size);
    }

    /**
     * Преобразует массив байт в длинное число
     * @param data Массив байт
     * @param offset Смещение в массиве
     * @param order Порядок следования бит
     * @return Динное число
     */
    static long bytesToLong(byte[] data, int offset, int size, ByteOrder order) {
        if (data == null)
            return 0;
        if (size < 0 || size > SIZE_LONG)
            throw new IllegalArgumentException("Size must be between 0 and " + SIZE_LONG);
        return ByteBuffer.allocate(SIZE_LONG)
                .order(order)
                .put(data, offset, size)
                .getLong(0);
    }

    /**
     * Преобразуем длинное число в массив байт
     * @param data Входное значение
     * @param size Количество байт считываемых из целого числа
     * @param order Порядок следования бит
     * @return Массив байт
     */
    public static byte[] longToBytes(long data, int size, ByteOrder order) {
        if (size < 0 || size > SIZE_LONG)
            throw new IllegalArgumentException("Size must be between 0 and " + SIZE_LONG);
        byte[] buff = ByteBuffer.allocate(SIZE_LONG)
                .order(order)
                .putLong(data)
                .array();
        return order == ByteOrder.BIG_ENDIAN ?
                Arrays.copyOfRange(buff, (SIZE_LONG - 1) - (size - 1), SIZE_LONG) :
                Arrays.copyOfRange(buff, 0, size);
    }

    /**
     * Выравнивание длиннны массива байт
     * @param data Входной массив байт
     * @param length Длина выравнивания
     * @return Выходной массив байт
     */
    public static byte[] alineBytes(byte[] data, int length) {
        if (data == null)
            return null;
        if (data.length > length)
            throw new IllegalArgumentException("Data size biggens of length");
        return ByteBuffer.allocate(length)
                .put(data)
                .array();
    }

    /**
     * Склеивает два массива байт
     * @param a Первый массив байт
     * @param b Второй массив байт
     * @return a + b
     */
    public static byte[] concatenateByteArrays(@NonNull byte[] a, @NonNull byte[] b) {
        byte[] result = new byte[a.length + b.length];
        System.arraycopy(a, 0, result, 0, a.length);
        System.arraycopy(b, 0, result, a.length, b.length);
        return result;
    }

    /**
     * Преобразует строку в массив байт в заданной кодировке
     * @param str Исходная строка
     * @param sizeLimit Ограничение на длинну строки или 0 для всей строки
     * @param encoding Кодировка результирующего массива байт
     * @return Результирующий массив байт
     */
    public static byte[] stringToBytes(String str, int sizeLimit, String encoding) {
        if (str == null)
            return null;
        if (sizeLimit == 0 || str.length() <= sizeLimit) {
            try {
                return str.getBytes(encoding);
            } catch (UnsupportedEncodingException e) {
                return BAD_ENCODING_STRING.getBytes();
            }
        } else {
            try {
                return str.substring(0, sizeLimit).getBytes(encoding);
            } catch (UnsupportedEncodingException e) {
                return BAD_ENCODING_STRING.getBytes();
            }
        }
    }

    /**
     * Преобразует набор массив байт в HEX строку
     */
    public static String bytesToHexString(byte[] bytes) {
        if (bytes == null)
            return "";
        StringBuilder stringBuilder = new StringBuilder(bytes.length * 3);
        for (byte b : bytes)
            stringBuilder.append(String.format("0x%02X ", b & 0xFF));
        return stringBuilder.toString().trim();
    }
}
