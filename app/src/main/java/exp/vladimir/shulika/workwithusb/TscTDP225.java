package exp.vladimir.shulika.workwithusb;

import android.content.Context;
import android.content.res.AssetManager;
import android.hardware.usb.UsbManager;
import android.util.Log;

import java.io.IOException;
import java.io.InputStream;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

/**
 * Created by shulika on 25.11.2016.
 */
public class TscTDP225 {

    int widthInDot = 344;
    int heightInDot = 200;
    public String lastCMD;
    Context context;
    DataTransfer printer;

    TscTDP225(DataTransfer io, Context context){

        this.context = context;
        printer = io;
        example_PrintLabel();
        //testLabel();
    }


    //==============================================================================================

    public void example_PrintLabel(){

        createCMDPrintLabel_43x25(291234569876L,
                "Марс,Равнина Элизий,ул. 50-летия годовщины д1іїґёЪ",
                "Щебень бутилированный (ГМО) « » \"іїґёЪ",
                "Состав: наилучшие намерения, вода пастеризованная, нанотехнологии 2шт",
                99.12,
                0,
                Calendar.getInstance().getTime(),
                Calendar.getInstance().getTime());

        writeUSB();

    }

    //==============================================================================================
    public byte[] testLabel(){

        byte[] res = null;

        try {

           /* String sbuf =  new String("CLS\r\n" +
                    "DIRECTION 1,0\r\n" +

                    "QRCODE 10,10,Q,4,A,0,M2,\"zmr2644@gmail.com\"\r\n" +

                    "TEXT 180,20,\"5\",0,2,2,\"44\"\r\n" +

                    "REVERSE 150,1,188,140\n" +

                    "TEXT 32,155,\"3\",0,1,1,\"zmr2644@gmail.com\"\r\n" +

                    "BOX 8,0,338,188,1\r\n" +

                    "PRINT 1\r\n");*/


                /*
                     String sbuf =  new String("CLS\r\n" +
                    "DIRECTION 1,0\r\n" +

                    "QRCODE 10,10,Q,4,A,0,M2,\"https://bitbucket.org/svavan/ww_ccs_031216\"\r\n" +

                     "TEXT  150,20,\"MSSaSe16\",0,1,1,\"CPU:   pic12f675\"\r\n" +
                     "TEXT  150,40,\"MSSaSe16\",0,1,1,\"SPEED: 4-20 Mhz\"\r\n" +
                     "TEXT  150,60,\"MSSaSe16\",0,1,1,\"ROM:   1.75 KB\"\r\n" +
                     "TEXT  150,80,\"MSSaSe16\",0,1,1,\"RAM:   64 B\"\r\n" +
                    "TEXT  150,100,\"MSSaSe16\",0,1,1,\"ADC:   10-bit\"\r\n" +
                    "TEXT  150,120,\"MSSaSe16\",0,1,1,\"OVR:   2-5.5V\"\r\n" +

                    "TEXT 20,170,\"MSSaSe8\",0,1,1,\"https://bitbucket.org/svavan/ww_ccs_031216\"\r\n" +

                    "BOX 8,0,338,188,1\r\n" +

                    "PRINT 1\r\n");*/

            String sbuf =  new String("CLS\r\n" + "DIRECTION 1,0\r\n");
            sbuf +="SIZE 43.00 mm,25.00 mm\r\n";
            sbuf +="GAP 2.00 mm,0.00 mm\r\n";
            sbuf +="DIRECTION 1,0\r\n";
            sbuf +="BOX 8,0,338,188,1\r\n";
            sbuf += "TEXT  10,4,\"MSSaSe8.fnt\",0,1,1,\""+ createTestStr(32,55).replace("\"","”") +"\"\r\n";
            sbuf += "TEXT  10,16,\"MSSaSe8.fnt\",0,1,1,\""+ createTestStr(87,55) +"\"\r\n";
            sbuf += "TEXT  10,28,\"MSSaSe8.fnt\",0,1,1,\""+ createTestStr(142,55) +"\"\r\n";
            sbuf += "TEXT  10,40,\"MSSaSe8.fnt\",0,1,1,\""+ createTestStr(197,44) +"\"\r\n";
            sbuf += "TEXT  10,51,\"MSSaSe8.fnt\",0,1,1,\""+ createTestStr(241,14) +"\"\r\n";
            sbuf += "TEXT  10,62,\"MSSaSe12.fnt\",0,1,1,\""+ "АаЛлЖжЩщЯяЭэ «»іїґёЪ" +"\"\r\n";
            sbuf += "TEXT  10,77,\"MSSaSe16.fnt\",0,1,1,\""+ "АаЛлЖжЩщЯяЭэ «»іїґёЪ" +"\"\r\n";
            sbuf += "TEXT  10,97,\"MSSaSe24.fnt\",0,1,1,\""+ "АаЛлЖжЩщЯяЭэ «»іїґёЪ" +"\"\r\n";

            //sbuf += "TEXT  10,94,\"MSSaSe24.fnt\",0,1,1,\""+ "НЕ ИСПОЛЬЗОВАТЬ!" +"\"\r\n";

            sbuf += "TEXT  10,130,\"MSSaSe24.fnt\",0,1,1,\""+ "«»іїґёЪ" +"\"\r\n";
            sbuf += "PRINT 1\r\n";

            lastCMD = "\r\n" + sbuf;

            res = sbuf.getBytes("cp1251");
        }
        catch (Exception ex)
        {
            Log.d("TscTDP225", "Error(testLabel): " + ex.toString());
            return  null;
        }

        return  res;
    }
    //==============================================================================================

    String createTestStr(int start, int lenght){

        try {
            byte[] buf = new byte[lenght];

            for (int i = 0; i < lenght; i++) buf[i] = (byte) (i + start);//buf+=String.format("%c", i);

            return new String(buf, "cp1251");
        }
        catch (Exception ex)
        {
            Log.d("TscTDP225", "Error(createTestStr): " + ex.toString());
            return  null;
        }

    };

    //==============================================================================================
    public byte[] example_createCMDPrintQRCode(String msg,int size){

        byte[] res = null;

        try {

            String sbuf =  new String("CLS\r\n" +
                    "DIRECTION 1,0\r\n" +

                    "QRCODE 10,10,Q,"+ size + ",A,0,M2,\"" + msg + "\"\r\n" +

                    //"QRCODE  10,10,Q,7,M,0,M2,S3,\"N123456!ATHE  FIRMWARE  HAS  BEEN\"\r\n" +

                    "PRINT 1\r\n");

            lastCMD = "\r\n" + sbuf;

            res = sbuf.getBytes("cp1251");
        }
        catch (Exception ex)
        {
            Log.d("TscTDP225", "Error(createCMDPrintQRCode): " + ex.toString());
            return  null;
        }

        return  res;
    }

    //==============================================================================================
    public byte[] example_createCMDPrintDmatrixBarcode(int x, int y, int width, int height, String str){

        byte[] res = null;

        try {

            String sbuf =  new String("CLS\r\n" +
                    "DIRECTION 1,0\r\n" +

                    "DMATRIX " + x + "," + y + "," + width + "," + height +  ",\"" + str + "\"\r\n" +

                    "PRINT 1\r\n");

            lastCMD = "\r\n" + sbuf;

            res = sbuf.getBytes("cp1251");
        }
        catch (Exception ex)
        {
            Log.d("TscTDP225", "Error(createCMDPrintDmatrixBarcode): " + ex.toString());
            return  null;
        }

        return  res;
    }

    //==============================================================================================
    byte [] createCMDPrintLabel_43x25(long barCode,
                                      String address,
                                      String name,
                                      String textStr,
                                      double price,
                                      double weight,
                                      Date date1,
                                      Date date2)
    {

        if(address==null)address=" ";
        if(name==null)name=" ";
        if(textStr==null)textStr=" ";

        if((barCode>299999999999L)||(barCode<200000000000L)) return  null;
        if(address.length()>50)address=address.substring(0,50);
        if(name.length()>40)name=name.substring(0,40);
        if(textStr.length()>72)textStr=textStr.substring(0,72);

        address = address.replace("\"","”");
        name = name.replace("\"","”");
        textStr = textStr.replace("\"","”");

        String nameStr1 = " ";
        String nameStr2 = " ";
        String textStr1 = " ";
        String textStr2 = " ";

        if(name.length()>20){
            nameStr1 = name.substring(0,20);
            nameStr2 = name.substring(20,name.length());
        }
        else {
            nameStr1 = name;
        }

        if(textStr.length()>36){
            textStr1 = textStr.substring(0,36);
            textStr2 = textStr.substring(36,textStr.length());
        }
        else
            {
            textStr1 = textStr;
        }

        double calcWeight = weight;

        if(calcWeight==0)
            calcWeight = (barCode % 10000)/1000.0;

        double sum = calcWeight * price;

        DateFormat df = new SimpleDateFormat("HH:mm:ss dd/MM/yyyy");

        byte[] res = null;

        try {

            String sbuf =  new String("CLS\r\n" +

                    "SIZE 43.00 mm,25.00 mm\r\n" +
                    "GAP 2.00 mm,0.00 mm\r\n" +
                    "DIRECTION 1,0\r\n" +

                    "BOX 8,0,338,188,1\r\n" +
                    "BOX 216,116,338,188,1\r\n" +

                    "BARCODE  22,116,\"EAN13\",48,1,0,2,2,\""+ String.format("%012d",barCode) +"\"\r\n" +

                    "TEXT  12,4,\"MSSaSe8.fnt\",0,1,1,\""+ address +"\"\r\n" +

                    "TEXT  12,15,\"MSSaSe16.fnt\",0,1,1,\"" + nameStr1 + "\"\r\n" +
                    "TEXT  12,34,\"MSSaSe16.fnt\",0,1,1,\"" + nameStr2 + "\"\r\n" +

                    "TEXT  12,59,\"MSSaSe8.fnt\",0,1,1,\"" + textStr1 + "\"\r\n" +
                    "TEXT  12,69,\"MSSaSe8.fnt\",0,1,1,\"" + textStr2 + "\"\r\n" +

                    "TEXT  200,80,\"MSSaSe12.fnt\",0,1,1,\"ВЕС(кг.)\"\r\n" +
                    "TEXT  200,96,\"MSSaSe12.fnt\",0,1,1,\""+ String.format("%.3f",calcWeight) + "\"\r\n" +

                    "TEXT  268,80,\"MSSaSe12.fnt\",0,1,1,\"ЦЕНА(р.)\"\r\n" +
                    "TEXT  268,96,\"MSSaSe12.fnt\",0,1,1,\""+ String.format("%.2f",price) +"\"\r\n" +

                    "TEXT  219,120,\"MSSaSe16.fnt\",0,1,1,\"СУММА(р.)\"\r\n" +
                    "TEXT  222,150,\"MSSaSe16.fnt\",0,1,1,\""+ String.format("%.2f",sum) +"\"\r\n" +

                    "TEXT  12,80,\"MSSaSe12.fnt\",0,1,1,\""+ (date1 != null ? df.format(date1) : "") +"\"\r\n" +
                    "TEXT  12,96,\"MSSaSe12.fnt\",0,1,1,\""+ (date2 != null ? df.format(date2) : "") +"\"\r\n" +

                    "REVERSE 216,116,122,72\n" +

                    "PRINT 1\r\n");


            /*String fntName = "micross.TTF"; // " + fntName + "

             sbuf =  new String("CLS\r\n" +
                    "DIRECTION 1,0\r\n" +

                    "CODEPAGE 1251\r\n" +

                    "BOX 8,0,338,188,1\r\n" +
                    "BOX 216,116,338,188,1\r\n" +

                    "BARCODE  22,116,\"EAN13\",48,1,0,2,2,\""+ String.format("%012d",barCode) +"\"\r\n" +

                    "TEXT  12,4,\"" + fntName + "\",0,4,4,\""+ address +"\"\r\n" +

                    "TEXT  12,15,\"" + fntName + "\",0,8,8,\"" + nameStr1 + "\"\r\n" +
                    "TEXT  12,34,\"" + fntName + "\",0,8,8,\"" + nameStr2 + "\"\r\n" +

                    "TEXT  12,59,\"" + fntName + "\",0,4,4,\"" + textStr1 + "\"\r\n" +
                    "TEXT  12,69,\"" + fntName + "\",0,4,4,\"" + textStr2 + "\"\r\n" +

                    "TEXT  200,80,\"" + fntName + "\",0,6,6,\"ВЕС(кг.)\"\r\n" +
                    "TEXT  200,96,\"" + fntName + "\",0,6,6,\""+ String.format("%.3f",calcWeight) + "\"\r\n" +

                    "TEXT  268,80,\"" + fntName + "\",0,6,6,\"ЦЕНА(р.)\"\r\n" +
                    "TEXT  268,96,\"" + fntName + "\",0,6,6,\""+ String.format("%.2f",price) +"\"\r\n" +

                    "TEXT  219,120,\"" + fntName + "\",0,8,8,\"СУММА(р.)\"\r\n" +
                    "TEXT  222,150,\"" + fntName + "\",0,8,8,\""+ String.format("%.2f",sum) +"\"\r\n" +

                    "TEXT  12,80,\"" + fntName + "\",0,6,6,\""+ (date1 != null ? df.format(date1) : "") +"\"\r\n" +
                    "TEXT  12,96,\"" + fntName + "\",0,6,6,\""+ (date2 != null ? df.format(date2) : "") +"\"\r\n" +

                    "REVERSE 216,116,122,72\n" +

                    "PRINT 1\r\n");*/


            lastCMD = "\r\n" + sbuf;

            res = sbuf.getBytes("cp1251");
        }
        catch (Exception ex)
        {
            Log.d("TscTDP225", "Error(createCMDPrintLabel_43x25): " + ex.toString());
            return  null;
        }

        return  res;
    }
    //===============================================================================================

    public boolean writeUSB(){return writeUSB(null);}
    public boolean writeUSB(byte [] pack){

        try{

            if(pack!=null)
                printer.write(pack);
            else
                printer.write(lastCMD.getBytes("cp1251"));

            return true;

            }
        catch (Exception ex)
            {
                Log.d("TscTDP225", "Error(writeUSB): " + ex.toString());
            }

        return false;

    }
//-------------------------------------------------------------------------------------------------------------
    public String readUSB(int timeout, int count){

        String sRes="";

        if(count==0)count = 0x7FFFFFFF;

        try{

            byte [] buf;

            while(sRes.length()<count){

                buf = new byte[] { (byte)(printer.read(timeout))};
                if(buf[0]!=0)
                    sRes += new String(buf);
            }

            return sRes;

        }
        catch (Exception ex)
        {
            Log.d("TscTDP225", "Error: " + ex.toString());
        }

        return sRes;
    }
    //==============================================================================================
    public String getFileList(){

        String res = "";

        try {

            String sbuf =  new String("\r\nDIAGNOSTIC INTERFACE USB\r\n" + "DIAGNOSTIC REPORT FFILELIST\r\n");
            lastCMD = sbuf;
            writeUSB(null);

            res = readUSB(200,0);
        }
        catch (Exception ex)
        {
            Log.d("TscTDP225", "Error(getFileList): " + ex.toString());
        }

        return  res;
    }

    //==============================================================================================
    public boolean checkFileExist(String fileName){

        try {

            if((fileName==null)||(fileName.length()==0))
                throw new Exception("FileName error.");

            if(getFileList().contains(fileName))return true;

        }
        catch (Exception ex)
        {
            Log.d("TscTDP225", "Error(checkFileExist): " + ex.toString());
        }

        return  false;
    }

    //==============================================================================================
    public boolean checkAllFontExist(){

        try {

            String fList = getFileList();

            if(!fList.contains("MSSaSe8.fnt"))return false;
            if(!fList.contains("MSSaSe12.fnt"))return false;
            if(!fList.contains("MSSaSe16.fnt"))return false;
            if(!fList.contains("MSSaSe24.fnt"))return false;
            //if(!fList.contains("test.txt"))return false;

            if(getFreeSpaceOnFlash()!=2502)return false; //Проверка по свободному месту

            return true;

        }
        catch (Exception ex)
        {
            Log.d("TscTDP225", "Error(checkAllFontExist): " + ex.toString());
        }

        return  false;
    }

    //==============================================================================================
    public boolean loadFile(String fileName){

        try
        {
            if((fileName==null)||(fileName.length()==0))
                throw new Exception("FileName error.");

            AssetManager am = context.getAssets();

            InputStream is = am.open(fileName);

            byte [] bin = new byte[is.available()];
            int readBytesCount =  is.read(bin);
            is.close();

            String sbuf =  new String("DOWNLOAD F,\"" + fileName + "\"," + readBytesCount + ",");

            lastCMD = sbuf;

            writeUSB(null);

            writeUSB(bin);

            if(checkFileExist(fileName))
                return true;


        }
        catch (Exception ex)
        {
            Log.d("TscTDP225", "Error(loadFile): " + ex.toString());
        }

        return  false;
    }

    //==============================================================================================
    public boolean loadFontsInTDP225(){

        try {

            loadFile("MSSaSe8.fnt");
            if(!checkFileExist("MSSaSe8.fnt"))return false;

            loadFile("MSSaSe12.fnt");
            if(!checkFileExist("MSSaSe12.fnt"))return false;

            loadFile("MSSaSe16.fnt");
            if(!checkFileExist("MSSaSe16.fnt"))return false;

            loadFile("MSSaSe24.fnt");
            if(!checkFileExist("MSSaSe24.fnt"))return false;

            return true;

        }
        catch (Exception ex)
        {
            Log.d("TscTDP225", "Error(loadFontsInTDP225): " + ex.toString());
        }

        return  false;

    };

    //==============================================================================================
    public int getFreeSpaceOnFlash(){

        int freeSpaceInByte = -1;
        String res = "";

        try {

            res =  execAdvancedCommand("DIAGNOSTIC REPORT FFREESPACE");

            if(res==null)return -1;
            if (res.length()<5)return -1;

           res = res.replaceAll("[^0-9]+", " ");

           String [] arrS = res.split(" ");

           freeSpaceInByte = Integer.parseInt(arrS[arrS.length-1]);

            return freeSpaceInByte;
        }
        catch (Exception ex)
        {
            Log.d("TscTDP225", "Error(getFreeSpaceOnFalsh): " + ex.toString());
        }

        return  -1;

    }

    //==============================================================================================

    public String dellAllFiles(){
       return execAdvancedCommand("KILL F,\"*.*\"");
    };

    //==============================================================================================

    public String execAdvancedCommand(String cmd){

        String res = "";

        try {

            if((cmd==null)||(cmd.length()==0))
                throw new Exception("Input command error.");

            String sbuf =  new String("\r\nDIAGNOSTIC INTERFACE USB\r\n" + cmd + "\r\n");
            lastCMD = sbuf;
            writeUSB(null);

            res = readUSB(200,0);
        }
        catch (Exception ex)
        {
            Log.d("TscTDP225", "Error(execAdvancedCommand): " + ex.toString());
        }

        return  res;

        //===============================================================================
        /* Список "секретных" комманд добытый из exe-шника виндовой программы настройки.
        //===============================================================================

            DIAGNOSTIC INTERFACE USB  - выбор интерфейса по которому принтер будет воспринимать диаг. команды
            DIAGNOSTIC INTERFACE COM
            DIAGNOSTIC INTERFACE LPT
            DIAGNOSTIC INTERFACE NET

             DIAGNOSTIC REPORT DPI
             DIAGNOSTIC REPORT VERSION
             DIAGNOSTIC REPORT MILAGE
             DIAGNOSTIC REPORT CHECKSUM
             OUT _SERIAL$
             DIAGNOSTIC REPORT SPEED
             DIAGNOSTIC REPORT DENSITY
             DIAGNOSTIC REPORT DENSITYBASE
             DIAGNOSTIC REPORT PAPERWIDTH
             DIAGNOSTIC REPORT PAPERHEIGHT
             DIAGNOSTIC REPORT SENSORMODE
             DIAGNOSTIC REPORT GAPSIZE
             DIAGNOSTIC REPORT GAPOFFSET
             DIAGNOSTIC REPORT PRINTOUT
             DIAGNOSTIC REPORT CUTTERPIECE
             DIAGNOSTIC REPORT REFERENCEX
             DIAGNOSTIC REPORT REFERENCEY
             DIAGNOSTIC REPORT DIRCTION
             DIAGNOSTIC REPORT MIRROR
             DIAGNOSTIC REPORT OFFSET
             DIAGNOSTIC REPORT OFFSETBASE
             DIAGNOSTIC REPORT SHIFT
             DIAGNOSTIC REPORT SHIFTBASE
             DIAGNOSTIC REPORT RIBBON
             DIAGNOSTIC REPORT CODEPAGE
             DIAGNOSTIC REPORT COUNTRY
             DIAGNOSTIC REPORT HEAD
             DIAGNOSTIC REPORT GAPINTEN
             DIAGNOSTIC REPORT BLINEINTEN
             DIAGNOSTIC REPORT CONINTEN
             DIAGNOSTIC REPORT BAUDRATE
             DIAGNOSTIC REPORT DATABIT
             DIAGNOSTIC REPORT PARITY
             DIAGNOSTIC REPORT STOPBIT
             DIAGNOSTIC REPORT REPRINT
             DIAGNOSTIC REPORT SHIFTX
             DIAGNOSTIC REPORT SHIFTXBASE
             DIAGNOSTIC REPORT EDITION
             DIAGNOSTIC REPORT ZDARKNESS
             DIAGNOSTIC REPORT ZSPEED
             DIAGNOSTIC REPORT ZPOSITION
             DIAGNOSTIC REPORT ZPRINTMODE
             DIAGNOSTIC REPORT ZLABELWIDTH
             DIAGNOSTIC REPORT ZCONTROL
             DIAGNOSTIC REPORT ZFORMAT
             DIAGNOSTIC REPORT ZDELIMITER
             DIAGNOSTIC REPORT ZFEEDPOWERUP
             DIAGNOSTIC REPORT ZFEEDHEADCLOSE
             DIAGNOSTIC REPORT ZLABELTOP
             DIAGNOSTIC REPORT ZLABELSHIFT
             DIAGNOSTIC REPORT DHEAT
             DIAGNOSTIC REPORT DSPEED
             DIAGNOSTIC REPORT DLABELWIDTH
             DIAGNOSTIC REPORT DPRESENTSENSOR
             DIAGNOSTIC REPORT DCUTTEREQUIPPED
             DIAGNOSTIC REPORT DCONTROLCODES
             DIAGNOSTIC REPORT DCOLUMNOFFSET
             DIAGNOSTIC REPORT DROWOFFSET
             DIAGNOSTIC REPORT CUTCOUNTER
             DIAGNOSTIC REPORT MAXLENGTH
             DIAGNOSTIC REPORT RESETMILAGE
             DIAGNOSTIC REPORT RESETCUTCOUNTER
             DIAGNOSTIC REPORT RIBBONSENSOR
             DIAGNOSTIC REPORT RIBBONENCODER
             DIAGNOSTIC REPORT AUTOTHRESHOLD
             DIAGNOSTIC REPORT ISBT
             DIAGNOSTIC REPORT BTPINCODE
             DIAGNOSTIC REPORT BTNAME
             DIAGNOSTIC REPORT ISWIFI
             DIAGNOSTIC REPORT WIFISSID
             DIAGNOSTIC REPORT WIFIWPA
             DIAGNOSTIC REPORT WIFIWEP
             DIAGNOSTIC REPORT WIFIKEYINDEX
             DIAGNOSTIC REPORT WIFIIP
             DIAGNOSTIC REPORT WIFIMASK
             DIAGNOSTIC REPORT WIFIGATEWAY
             DIAGNOSTIC REPORT ISWIFI
             DIAGNOSTIC REPORT WIFISSID
             DIAGNOSTIC REPORT WIFIWPA
             DIAGNOSTIC REPORT WIFIWEP
             DIAGNOSTIC REPORT WIFIKEYINDEX
             DIAGNOSTIC REPORT WIFIIP
             DIAGNOSTIC REPORT WIFIMASK
             DIAGNOSTIC REPORT WIFIGATEWAY
             DIAGNOSTIC REPORT EDITION
             DIAGNOSTIC REPORT PASSWORD
             DIAGNOSTIC PASSWORD "%s"
             SPEED %.1f
             DENSITY %d
             SIZE %.2f mm,%.2f mm
             SIZE %.2f,%.2f
             GAP %.2f mm,%.2f mm
             GAP %.2f,%.2f
             BLINE %.2f mm,%.2f mm
             BLINE %.2f,%.2f
             GAP 0,0
             SET TEAR OFF
             SET PEEL OFF
             SET CUTTER OFF
             SET TEAR ON
             SET PEEL OFF
             SET CUTTER OFF
             SET TEAR OFF
             SET PEEL ON
             SET CUTTER OFF
             SET TEAR OFF
             SET PEEL OFF
             SET CUTTER %d
             SET TEAR OFF
             SET PEEL OFF
             SET CUTTER 1
             REFERENCE %d,%d
             DIRECTION %d,%d
             DIRECTION %d
             OFFSET %d dot
             SHIFT %d,%d
             SHIFT %d
             SET RIBBON OFF
             SET RIBBON ON
             SET RIBBONEND OFF
             SET RIBBONEND ON
             SET ENCODER OFF
             SET ENCODER ON
             CODEPAGE %s
             COUNTRY %s
             SET HEAD OFF
             SET HEAD ON
             SET REPRINT OFF
             SET REPRINT ON
             LIMITFEED %.2f dot
             SET GAP %d
             SET GAP %d
             SET GAP %d
             SET SENSOR_REF MANUAL
             SET SENSOR_REF AUTO
             SET COM1 %s,%c,%c,%c
             BT PINCODE "%s"
             BT NAME "%s"
             WLAN SSID ""
             WLAN SSID "%s"
             WLAN WPA ""
             WLAN WPA "%s"
             WLAN WEP1 ""
             WLAN WEP%d "%s"
             0.0.0.0 WLAN DHCP
             WLAN IP "%s","%s","%s"
             WLAN SSID ""
             WLAN SSID "%s"
             WLAN WPA OFF
             WLAN WPA "%s"
             WLAN WEP OFF
             WLAN WEP %d,"%s"
             0.0.0.0 WLAN DHCP
             WLAN IP "%s","%s","%s"
             DUMP_TEXT PRINT, DUMP_TEXT SAVE 1000,E,"%s", DUMP_TEXT SAVE 1000,"Dump.txt", @YEAR = "%d"
             @MONTH = "%d"
             @DATE = "%d"
             @HOUR = "%d"
             @MINUTE = "%d"
             @SECOND = "%d"
             DIAGNOSTIC REPORT YEAR
             DIAGNOSTIC REPORT MONTH
             DIAGNOSTIC REPORT DATE
             DIAGNOSTIC REPORT HOUR
             DIAGNOSTIC REPORT MINUTE
             DIAGNOSTIC REPORT SECOND
             SET SENSOR_REF MANUAL
             GAPDETECT %d,%d
             BLINEDETECT %d,%d
             AUTODETECT %d,%d
             SET SENSOR_REF AUTO
             GAPDETECT
             BLINEDETECT
             GAP 0.12,0
             GAP 0,0
             AUTODETECT
             SET SENSOR_REF MANUAL
             GAP 0.08,0
             BLINE 0.08,0
             GAP 0,0
             SET GAP %d,%d
             SET GAP %d
             JL DIAGNOSTIC REPORT DPHYSPACE
             DIAGNOSTIC REPORT DFREESPACE  D - Dram; F - Flash; C - Card (SDcard)
             DIAGNOSTIC REPORT DFILELIST
             DIAGNOSTIC REPORT FPHYSPACE
             DIAGNOSTIC REPORT FFREESPACE
             DIAGNOSTIC REPORT FFILELIST
             DIAGNOSTIC REPORT CFILELIST
             DOWNLOAD %s"%s",%d, KILL "*.*"
             KILL F,"*.*"                       удалить все файлы на Falsh
             KILL E,"*.*"
             CLS
             DIRECTION 0,0
             BOX %d,%d,%d,%d,4
             BARCODE  %d,%d,"39",%d,1,90,%d,%d,"%s"
             TEXT  %d,%d,"0",0,%d,%d,"TEST"
             PRINT 1
             NET DHCP
             NET IP "%s","%s","%s"
             NET NAME "%s"
             DIAGNOSTIC REPORT CONFIGIP
             DIAGNOSTIC REPORT GATEWAY
             DIAGNOSTIC REPORT IPMASK
             DIAGNOSTIC REPORT LASTUSEDIP
             DIAGNOSTIC REPORT NETNAME
             DIAGNOSTIC REPORT MACADDR
             DIAGNOSTIC FILE "%s"
             DIAGNOSTIC REPORT GAPINTEN
             DIAGNOSTIC REPORT BLINEINTEN
             DIAGNOSTIC REPORT CONINTEN
             DIAGNOSTIC REPORT SENSORMODE
             DIAGNOSTIC REPORT EDITION
             DIAGNOSTIC REPORT THRESHOLD
             DIAGNOSTIC REPORT BLINESENSOR
             DIAGNOSTIC REPORT BLINESENSOR
             DIAGNOSTIC REPORT GAPSENSOR
             DIAGNOSTIC REPORT GAPSENSOR

*/
    }


}
