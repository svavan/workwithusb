package exp.vladimir.shulika.workwithusb.tools;

import java.io.Closeable;
import java.io.IOException;
import java.math.BigDecimal;
import java.math.RoundingMode;

/**
 * Разное
 */
public class Common {
    /**
     * Заквает поток без обработки исключения
     * @param closeable Поток
     */
    public static void silentClose(Closeable closeable) {
        if (closeable == null) return;
        try {
            closeable.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    /**
     * Сон без обработки исключений
     * @param mils Время сна в миллисекундах
     */
    public static void silentSleep(long mils) {
        try {
            Thread.sleep(mils);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }

    /**
     * Округление Double до заданного числа десятичных разрядов вверх
     * @param value Значение
     * @param places Число десятичных разрядов
     * @return Округленная сумма
     */
    public static double roundUp(double value, int places) {
        if (places < 0) throw new IllegalArgumentException();
        BigDecimal bd = new BigDecimal(value);
        bd = bd.setScale(places, RoundingMode.HALF_UP);
        return bd.doubleValue();
    }
}
