package exp.vladimir.shulika.workwithusb;

import android.hardware.usb.*;
import android.util.Log;
import exp.vladimir.shulika.workwithusb.DataTransfer;
import exp.vladimir.shulika.workwithusb.tools.Bytes;
import exp.vladimir.shulika.workwithusb.tools.Common;

import java.io.IOException;
import java.nio.ByteOrder;
import java.util.Queue;
import java.util.concurrent.ConcurrentLinkedQueue;

/**
 * Протокол обмена с TscTDP225 через USB
 */
public class TscTDP225USBPort implements DataTransfer, Runnable {
    private static final int    READ_SLEEP         =  10;

    private final UsbDevice usbDevice;
    private final UsbManager usbManager;

    private final UsbInterface iface;
    private final UsbEndpoint inEndPoint;
    private final UsbEndpoint outEndPoint;

    private UsbDeviceConnection connection;
    private volatile boolean isRun;
    private volatile boolean busy;
    private Thread thread;
    private volatile Queue<Byte> readBuffer;


    //==============================================================================================
    public TscTDP225USBPort(int vendorId, int productId, UsbManager usbManager) {

        UsbDevice res = null;

        for (UsbDevice usbDevice : usbManager.getDeviceList().values()) {
            if ((usbDevice.getVendorId() == vendorId) && (usbDevice.getProductId() == productId))
                res = usbDevice;
        }

        this.usbDevice = res;
        this.usbManager = usbManager;
        UsbEndpoint inEndPoint = null;
        UsbEndpoint outEndPoint = null;
        iface = usbDevice.getInterface(0);
        for (int i = 0; i < iface.getEndpointCount(); i++) {
            UsbEndpoint endPoint = iface.getEndpoint(i);
            if (endPoint.getType() == UsbConstants.USB_ENDPOINT_XFER_BULK)
                if (endPoint.getDirection() == UsbConstants.USB_DIR_IN) {
                    inEndPoint = endPoint;
                } else if (endPoint.getDirection() == UsbConstants.USB_DIR_OUT) {
                    outEndPoint = endPoint;
                }
        }
        this.inEndPoint = inEndPoint;
        this.outEndPoint = outEndPoint;
        this.busy = false;
    }

    /**
     * Инициализация USB порта
     * @param usbDevice USB устройство
     * @param usbManager USB менеджер
     */

    public TscTDP225USBPort(UsbDevice usbDevice, UsbManager usbManager) {
        this.usbDevice = usbDevice;
        this.usbManager = usbManager;
        UsbEndpoint inEndPoint = null;
        UsbEndpoint outEndPoint = null;
        iface = usbDevice.getInterface(0);
        for (int i = 0; i < iface.getEndpointCount(); i++) {
            UsbEndpoint endPoint = iface.getEndpoint(i);
            if (endPoint.getType() == UsbConstants.USB_ENDPOINT_XFER_BULK)
                if (endPoint.getDirection() == UsbConstants.USB_DIR_IN) {
                    inEndPoint = endPoint;
                } else if (endPoint.getDirection() == UsbConstants.USB_DIR_OUT) {
                    outEndPoint = endPoint;
                }
        }
        this.inEndPoint = inEndPoint;
        this.outEndPoint = outEndPoint;
        this.busy = false;
    }

    @Override
    public void open() throws IOException {
        readBuffer = new ConcurrentLinkedQueue<>();
        connection = usbManager.openDevice(usbDevice);

        if (connection.claimInterface(iface, true)) {
            thread = new Thread(this, "TscTDP225 USB port processor");
            thread.start();
        }

    }

    @Override
    public void write(byte[] data) throws IOException {
        // Отправляем данные
        //connection.bulkTransfer(outEndPoint, data, data.length, 0);
        //Log.d("TscTDP225", "WriteSize: " + data.length + " bytes.");

        if(data.length<0x4000) {
            int bytesCountWrite = connection.bulkTransfer(outEndPoint, data, data.length, 0);
            Log.d("TscTDP225(writeEx)", "WriteSize: " + data.length + " bytes. Write: " + bytesCountWrite);
            //return bytesCountWrite;
        }
        else
        {
            writeByParts(data,0x4000);
        }
        //return -1;
    }

    //==========================================================================================================
    /*
    public int writeEx(byte[] data) throws IOException {
        // Отправляем данные

        if(data.length<0x4000) {
            int bytesCountWrite = connection.bulkTransfer(outEndPoint, data, data.length, 0);
            Log.d("TscTDP225(writeEx)", "WriteSize: " + data.length + " bytes. Write: " + bytesCountWrite);
            return bytesCountWrite;
        }
        else{
          if(writeByParts(data,0x4000))return data.length;
        }
        return -1;
    }
    */
    //==========================================================================================================

    public boolean writeByParts(byte[] data, int sizeOfPart) {

        byte[] buf;

        int i;

        for(i=0;i<data.length;i=i+sizeOfPart)
        {
            if((data.length-i)<sizeOfPart)
            {
                buf = new byte[data.length-i];
            }
            else
            {
                buf = new byte[sizeOfPart];
            }

            System.arraycopy(data,i,buf,0,buf.length);

            int bytesCountWrite = connection.bulkTransfer(outEndPoint, buf, buf.length, 0);
            Log.d("TscTDP225(writeByParts)", "WriteSize: " + buf.length + " bytes. Write: " + bytesCountWrite);
            if(bytesCountWrite != buf.length)return false;

        }
        return true;
    }

    //==========================================================================================================

    @Override
    public int read(int timeout) throws IOException {
        int time = 0;
        while (readBuffer.isEmpty()) {
            Common.silentSleep(READ_SLEEP);
            if (busy)
                time = 0;
            else
                time += READ_SLEEP;
            if ((timeout > 0 && time >= timeout))
                throw new IOException("Read timeout");
        }
        return readBuffer.poll();
    }

    @Override
    public String portId() {
        return usbDevice.getDeviceName();
    }

    @Override
    public void close() throws IOException {
        connection.releaseInterface(iface);
        connection.close();
        isRun = false;
    }

    @Override
    public void run() {
        Log.d("TscTDP225", "Thread TscTDP225 USB port processor start.");
        byte[] buffer = new byte[inEndPoint.getMaxPacketSize()];
        isRun = true;
        while (isRun)
        {
           int readSize  = connection.bulkTransfer(inEndPoint, buffer, buffer.length, 5);
           if(readSize>=0)
           {
               for (int i = 0; i < readSize; i++) readBuffer.add(buffer[i]);
               //dataSize -= readSize;
               Log.d("TscTDP225", "ReadSize: " + readSize);
           }

            Common.silentSleep(50);
        }
    }

    /**
     * Выполняет управляющий запрос USB
     * @param request Код запроса
     * @return Ответ
     */

    private int controlRequest(int request) {
        byte[] buffer = new byte[2];
        connection.controlTransfer(0xc1, request, 0x0000, 0x0000, buffer, 2, 0);
        return Bytes.bytesToInt(buffer, 0, 2, ByteOrder.LITTLE_ENDIAN);
    }

    /*private byte[] initialRequest() {
        byte[] buffer = new byte[0x3f1];


        //connection.controlTransfer(0x80, 0x06, 0x01, 0x0002, buffer, 0x0109, 50);
        connection.controlTransfer(0x00, 0x09, 1, 0x0000, buffer, 0x0000, 100);
        //connection.controlTransfer(0x80, 0x06, 0x01, 0x0001, buffer, 0x0012, 50);
        //Запрашиваем инфу о принтере, ответ ниже.
        connection.controlTransfer(0xA1, 0, 0x0000, 0x0000, buffer, 0x3f1, 100);
        // Получаем строку:
        //00 2D 4D 46 47 3A 54 53 43 3B 43 4D 44 3A 54 53   .-MFG:TSC;CMD:TS
        //50 4C 32 3B 4D 44 4C 3A 54 44 50 2D 32 32 35 3B   PL2;MDL:TDP-225;
        //43 4C 53 3A 50 52 49 4E 54 45 52 3B 00            CLS:PRINTER;.
        // хз зачем, без этого принтер не отвечает на команды,
        // така длинна потому что так делает виндовый двайвер
        Log.d("TscTDP225", "InitialRequest: rcv. bytes count is (in bytes): " + buffer.length);
        return buffer;
    }*/

}
