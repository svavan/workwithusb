package exp.vladimir.shulika.workwithusb;

import java.io.Closeable;
import java.io.IOException;

/**
 * Обертка для обмена данными с физическим устройством
 */
public interface DataTransfer extends Closeable {
    /**
     * Открывает интерфейс
     * @throws IOException Проблема с вводом / выводом
     */
    void open() throws IOException;

    /**
     * Передать блока данных в интерфейс
     * @param data Блок данных
     * @throws IOException Проблема с вводом / выводом
     */
    void write(byte[] data) throws IOException;

    /**
     * Прочитать значение из интерфейса
     * @return Прочитанное значение
     * @throws IOException Проблема с вводом / выводом
     */
    int read(int timeout) throws IOException;

    /**
     * @return Идентификатор порта
     */
    String portId();
}
